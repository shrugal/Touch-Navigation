/** Distance (px) before a gesture direction is locked in */
const DISTANCE_START = 20;
/** Tolerance (px) for inaccuracy during a gesture */
const DISTANCE_TOLERANCE = 60;
/** Required distance (px) for a gesture to trigger an effect */
const DISTANCE_END = 200;
/** Icon radius (px) at the start of a gesture */
const RADIUS_START = 12;
/** Icon radius (px) at the end of a gesture */
const RADIUS_END = 20;
/** Icon position (px) at the start of a gesture */
const POSITION_START = -50;
/** Icon position (px) at the end of a gesture */
const POSITION_END = 60;
/** Max. depth to search for ignored element at touch position */
const IGNORE_MAX_DEPTH = 5;
/** Added to ids and classes to prevent messing with the page */
const SUFFIX = "h030a6agks";

/** Top, Left, Bottom, Right */
let gesture = [0, 0, 0, 0];
// Key values during a gesture
let startX = 0, startY = 0, dir = 0, dist = 0;
/** @type {SVGElement} */
let icon_arrow
/** @type {SVGElement} */
let icon_refresh;

/**
 * Start a gesture.
 * @param {HTMLElement} el
 * @param {number} x
 * @param {number} y
 */
function start(el, x, y) {
    // Check elements at touch position
    const els = document.elementsFromPoint(x, y);
    for (let i=0, el=els[0]; el && i<IGNORE_MAX_DEPTH; el=els[++i]) {
        if (el.tagName === "IFRAME" || el.tagName === "CANVAS") return;
        if (el.id === "map") return;
        if (el.tagName !== "DIV") break;
    }

    // Determine directions that cannot be scrolled anymore
    for (gesture = [1, 1, 1, 1]; el; el = el.parentElement) {
        gesture[0] &= el.scrollTop === 0;
        gesture[1] &= el.scrollLeft === 0;
        gesture[2] &= el.scrollTop === el.scrollTopMax;
        gesture[3] &= el.scrollLeft === el.scrollLeftMax;

        if (!gesture.some(Boolean)) return;
    }

    // Initialize gesture values
    startX = x, startY = y;
}

/**
 * Update gesture progress.
 * @param {number} x
 * @param {number} y
 */
function move(x, y) {
    const distX = Math.abs(x - startX), distY = Math.abs(y - startY);
    dir = distY > distX ? (y > startY ? 0 : 2) : (x > startX ? 1 : 3);
    dist = Math.max(distX, distY);
    
    // Lock in gesture direction
    if (dist >= DISTANCE_START && gesture.filter(Boolean).length > 1) {
        gesture = gesture.map((v, i) => v & dir === i);
        if (!gesture[dir]) return reset();
    }

    // Check gesture direction
    if (!gesture[dir]) {
        if (!gesture[(dir + 2) % 4] && dist >= DISTANCE_TOLERANCE) return reset();
        if (gesture.filter(Boolean).length !== 1) return removeIcons();
        dir = gesture.indexOf(1);
        dist = (dir < 2 ? 1 : -1)  * (dir % 2 ? x - startX : y - startY);
        if (dist < 0) return removeIcons();
    }
    
    const progress = Math.min(dist / DISTANCE_END, 1);
    const translate = POSITION_START + progress * (POSITION_END - POSITION_START);
    const radius = RADIUS_START + progress * (RADIUS_END - RADIUS_START);

    let icon, edge, rotate

    switch(dir) {
        case 0:
            // Top icon
            icon = getRefreshIcon();
            edge = "top";
            rotate = progress * 360;
            break;
        case 1: case 3:
            // Left/right icon
            icon = getArrowIcon();
            edge = dir === 3 ? "right" : "left";
            rotate = dir === 3 ? 180 : 0;
            break;
        default:
            return removeIcons();
    }

    icon.style.display = "initial";
    icon.style[edge] = `${translate}px`;
    icon.style.rotate = `${rotate}deg`;
    icon.classList.toggle("will-navigate", progress === 1);
    icon.querySelector("circle:not(:first-of-type)").setAttribute("r", radius);
}

/**
 * Finish gesture and trigger effect.
 */
function finish() {
    // Check gesture distance
    if (dist < DISTANCE_END) return reset();

    // Trigger appropriate action
    switch(dir) {
        case 0:
            location.reload();
            break;
        case 1: case 3:
            history[dir === 1 ? "back" : "forward"]();
            break;
    }

    reset();
}

/**
 * Cancel gesture.
 */
function reset() {
    gesture = [0, 0, 0, 0];
    removeIcons();
}

// -------- Inject --------

function getArrowIcon() {
    if (icon_arrow) return icon_arrow;

    return document.body.appendChild(
        icon_arrow = createIcon("arrow", "M37.6481 32.9253H29.2578L32.4489 29.7341C32.5321 29.6465 32.5778 29.5298 32.5763 29.4089C32.5747 29.288 32.526 29.1725 32.4405 29.087C32.355 29.0015 32.2395 28.9528 32.1186 28.9512C31.9977 28.9496 31.881 28.9954 31.7933 29.0786L27.7408 33.1334V33.6438L31.7941 37.6978C31.837 37.741 31.888 37.7753 31.9442 37.7987C32.0004 37.8221 32.0606 37.8341 32.1215 37.8341C32.1823 37.8341 32.2426 37.8221 32.2988 37.7987C32.355 37.7753 32.406 37.741 32.4489 37.6978C32.5354 37.6107 32.584 37.4929 32.584 37.3701C32.584 37.2473 32.5354 37.1294 32.4489 37.0423L29.2578 33.8512H37.6481C37.7709 33.8512 37.8887 33.8024 37.9755 33.7156C38.0623 33.6288 38.1111 33.511 38.1111 33.3882C38.1111 33.2654 38.0623 33.1477 37.9755 33.0609C37.8887 32.974 37.7709 32.9253 37.6481 32.9253Z")
    );
}

function getRefreshIcon() {
    if (icon_refresh) return icon_refresh;

    return document.body.appendChild(
        icon_refresh = createIcon("refresh", "M21,4.04a0.96,0.96 0,0 0,-0.96 0.96V8a8.981,8.981 0,1 0,-1.676 10.361A1,1 0,0 0,16.95 16.95a7.031,7.031 0,1 1,1.72 -6.91H15a0.96,0.96 0,0 0,0 1.92h6a0.96,0.96 0,0 0,0.96 -0.96V5A0.96,0.96 0,0 0,21 4.04Z")
    )
}

/**
 * @param {string} name
 * @param {string} path
 * @returns {SVGElement}
 */
function createIcon(name, path) {
    removeIcons();

    return new DOMParser().parseFromString(
        /* svg */ `
        <svg id="touch-swipe-${name}-icon-${SUFFIX}" class="touch-swipe-nav-icon-${SUFFIX}"
            width="66" height="66" viewBox="0 0 66 66" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <circle cx="33" cy="33" r="30" />
            <circle cx="33" cy="33" r="${RADIUS_START}" />
            <path d="${path}" />
        </svg>`,
        'image/svg+xml'
    ).firstElementChild;
}

function removeIcons() {
    icon_arrow = icon_arrow?.remove();
    icon_refresh = icon_refresh?.remove();
}

// -------- Events --------

document.addEventListener("touchstart", (e) => {
    if (e.eventPhase !== Event.BUBBLING_PHASE) return;
    if (e.touches.length !== 1) return;
    
    const { clientX, clientY } = e.touches[0];
    start(e.target, clientX, clientY);
});

document.addEventListener("touchmove", (e) => {
    if (!gesture.some(Boolean)) return;
    if (e.eventPhase !== Event.BUBBLING_PHASE) return;
    if (e.touches.length !== 1) return reset();

    const { clientX, clientY } = e.touches[0];
    move(clientX, clientY);
});

document.addEventListener("touchend", (e) => {
    if (!gesture.some(Boolean)) return;
    if (e.eventPhase !== Event.BUBBLING_PHASE) return;
    if (e.touches.length) return reset();

    finish();
});

document.addEventListener("touchcancel", reset);