import { hasPermissions, openPermissionsPage, closePermissionsPage } from "./permissions/util.js";

const NAME = browser.runtime.getManifest().name;

async function toggleAction() {
    const hasPerm = await hasPermissions();
    browser.action[hasPerm ? "disable" : "enable"]();
    browser.action.setTitle({ title: hasPerm ? NAME : `${NAME}: Grant Permissions` });
}

// -------- Exec --------

toggleAction();

// -------- Events --------

// Install/Update
browser.runtime.onInstalled.addListener(({ reason, previousVersion }) => {
    if (reason !== "install" && previousVersion !== "1.0") return;
    openPermissionsPage();
});

// Action
browser.action.onClicked.addListener(openPermissionsPage);

// Permission change
browser.permissions.onRemoved.addListener(toggleAction);
browser.permissions.onAdded.addListener(() => {    
    toggleAction();
    closePermissionsPage();
});