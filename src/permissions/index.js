import { requestPermissions } from "./util.js";

document.addEventListener("DOMContentLoaded", () => {
    document.querySelector("button").addEventListener("click", requestPermissions);
});