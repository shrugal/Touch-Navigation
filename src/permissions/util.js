/** @type {browser.permissions.AnyPermissions} */
export const PERMISSIONS = { origins: ["*://*/*"] };
export const URL = browser.runtime.getURL("src/permissions/index.html");

export function hasPermissions() {
    return browser.permissions.contains(PERMISSIONS);
}

export function requestPermissions() {
    return browser.permissions.request(PERMISSIONS);
}

export function closePermissionsPage() {
    browser.extension.getViews({ type: "tab" })
        .find(v => v.location.href === URL)
        ?.close();
}

export async function openPermissionsPage() {
    if (await hasPermissions()) return;
    closePermissionsPage();
    await browser.tabs.create({ url: URL, active: true });
}