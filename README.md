# Touch-Navigation

Add touch navigation gestures to go back/forward and reload the page in Firefox.

Install from: https://addons.mozilla.org/firefox/addon/touch-navigation/

## Description

This extension adds the following touch navigation gestures:

- Swipe right to go back
- Swipe left to go forward
- Swipe down to reload the page

It shows nice icons during the gesture and checks beforehand if you can still scroll in a given direction.

## Example

<p align="center">
    <img src="https://addons.mozilla.org/user-media/previews/full/296/296556.png" alt="Example" />
</p>